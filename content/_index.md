---
title: "Dog and Data"
featured_image: '/images/pipderp.jpg'
description: "Blog about data projects and my dog Pip."
---
Welcome to my blog. Here I hope to showcase data projects and to document my data science learning journey. Also, there will be random pics of my best friend Pip who is not happy about this site. He would rather I spend more time with him. 
