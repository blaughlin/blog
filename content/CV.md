---
title: Curriculum Vitae
featured_image: "images/notebook.jpg"
description: Bernard Laughlin
type: page
menu: main
---

# Bernard Laughlin
bernardlaughlin@gmail.com
## Education
Univeristy of Alaska Fairbanks   
Fairbanks, AK  
Ph.D. Biochemistry Canidate, 2017-2021  

Touro University College of Osteopathic Medicine  
Vallejo, CA  
D.O., 2008-2012

University of Washington  
Seattle, WA  
B.S. Economics, 1998-2001  
Certificate in Quantitative Managerial Economics

## Professional Training 
Post-Doctoral Fellow  
2014 - 2020   
Institute of Arctic Biology  
University Alaska Fairbanks  
Fairbanks, AK

Medical Intern  
2012 - 2013   
Chino Valley Medical Center  
Chino, CA 

## Certifications
| | |
| :---        |    :----   | 
|2014 - 2015|     Therapeutic Hypothermia Life Support|
|2012 - 2014|     Advanced Care Cardiac Life Support|
|2012 - 2014|     CPR|  
|           |           |

## Presentations
Spring 2017	  
Adenosine 1AR as a Novel Thermolytic Adjunctive Pharmacotherapy for Targeted Temperature Management. University of Alaska Biomedical Conference; Fairbanks, AK    

Spring 2017   
Targeting Central A1 Adenosine Receptors Induces Hypothermia for Ischemic Reperfusion Injury.        AIAN CTRP Spring Conference; Bozeman, MT. 

Spring 2016   
Targeted Temperature Management by Pharmacologically Inducing a Hibernation Like State. Aarhus University Hospital, Aarhus, Denmark 

Spring 2015   
A1 adenosine receptor targeted temperature management in rats and resultant physiological effects of a pharmacologically induced hypometabolic state. Hibernation 3.0 Conference; Duluth, MN

Fall 2014	   
Inhibition of thermogenesis in mammals and resultant arrhythmia via pharmacologically induced hypothermia by adenosine A1 receptor agonist. Arctic Division of the American Association for the Advancement of Science Conference; Fairbanks, AK

Spring 2012      
Acute Coronary Syndrome Early Management. Chino Valley Medical Center; Chino, CA

Spring 2012      
Nephrolithiasis Management. Chino Valley Medical Center; Chino, CA

Fall 2012    
Sialoithiasis Management. Chino Valley Medical Center; Chino, CA

Spring 2012   
Acute Hand Infections in the Emergency Department. Stanford University Medical Center; Palo Alto, CA

Fall 2011   
Pulmonary Renal Syndrome. Arrowhead Regional Medical Center Internal Medicine Department; Colton, CA

## Teaching
Fall 2020 / Spring 2021   
Graduate teaching assistant general chemistry. University of Alaska Fairbanks.

Summer 2016    
The Do and Don’ts of Data Management (TIDY Data).
Developed and co-taught this 2 week workshop on data management and the R programming language. University of Alaska Fairbanks

## Guest Lectures
Spring 2017   
Lecture on emotion and health.
Brain and Behavior PSY 335, Deparment of Psychology, UAF, Fairbanks, AK  

Spring 2015    
Lecture on inducing a hypometabolic state with an adenosine agonist. Neurochemistry CHEM F676, Department of Chemistry, UAF, Fairbanks, AK

Spring 2015   
Cerebrovascular Disease Management.
 Middle Cerebral Artery Occlusion training workshop, UAF, Fairbanks, AK.

## Career Development Training
Spring 2019   
NIH I-Corps Program. Houston, TX and Bethesda, MD

Spring 2018   
FDA Small Business & Industry Assistance Regulatory Education for Industry Conference. San Francisco, CA

Spring 2017   
Responsible Conduct of Research Workshop.
University of Alaska Fairbanks

Fall 2016   
Regression and Analysis of Variance Class (Stat 401).University of Alaska Fairbanks

Spring 2016   
Career Planning IDP Workshop.
University of Alaska Fairbanks

Fall 2015   
Introduction to the Principles and Practice of Clinical Research Class
NIH Clinical Center

Fall 2015   
Two day microsurgical workshop on abdominal aorta cannulation and telemetry transmitter implantation in rats. Data Sciences International

Summer 2015   
SBIR/STTR Grant Writing Workshop.
University of Alaska Fairbanks

Spring 2014   
Scholarly Writing Workshop. 
University of Alaska Fairbanks 

Fall 2013   
Human Health and Global Environmental Change Class.
HarvardX

## Abstracts/Poster
2015: 	Targeted temperature management with A1 adenosine agonist & surface temperature control in rats. Bailey IR, Bogren LK, Laughlin BL, Drew KL. Brain 2015 Conference, Vancouver, B.C.    

2015:	A study of bradycardia during 6N cyclohexyladenosine-induced cooling in rats. Dowell K, Laughlin B, Drew, K. Hibernation 3.0 Conference, Duluth, MN

2016: 	Targeted Temperature Management by Pharmacologically Inducing a Hibernation Like State. Laughlin B, Bailey I, Rice S, Drew K. Monitoring Molecules in Neuroscience, Gothenburg, Sweden

2016: 	Targeted Temperature Management by Pharmacologically Inducing a Hibernation Like State. Laughlin B, Bailey I, Rice S, Drew K. 15th International Hibernation Symposium, Las Vegas, NV

2016: 	Variable thermolytic response to A1 adenosine receptors in Rats. Bailey I, Laughlin B., Moore L., Rice S., Bhownicks S., Clement C., Duguay K., Murphy C., Bogren L., Drew K.  15th International Hibernation Symposium, Las Vegas, NV

2016: 	Analysis of Cardiac Arrhythmias During N6-Cyclohexyladenosine Assisted Cooling. Dowell K, Barati B, Laughlin B, Drew KL. 15th International Hibernation Symposium, Las Vegas, NV

2017:     N6Cyclohexyladenosine, an A1 Adenosine receptor agonist as a Novel Thermolytic, Adjunctive Pharmacotherapy, for Targeted Temperature Management. Laughlin B, Bailey I, Rice S, Barati B, Drew K. Experimental Biology Conference, Chicago, IL

2018:	Reversal of Peripheral and CNS Mediated A1 Adenosine Receptor Hypotension. Lauhglin B., Bailey I., Tagaban S., Drew K. American Society of Neurochemsitry Conference

2018:	Reversal of Peripheral and CNS Mediated A1 Adenosine Receptor Hypotension. Lauhglin B., Bailey I., Tagaban S., Drew K. Experimental Biology Conference, San Diego, CA

## Publications
Laughlin BW, Bailey IR, Rice SA, Barati Z, Bogren LK, Drew KL. “Precise Control of Target Temperature Using N6-Cyclohexyladenosine and Real-Time Control of Surface Temperature”. Ther Hypothermia Temp Manag. 2018 Doi 10.1089/ther.2017.0020. [Epub ahead of print]

Bailey IR, Laughlin B, Moore LA,, Bogren LK, Barati, B, Drew KL. “Optimization of THemolytic Response to A1 Adenosine Receptor Agonist in Rats”. J Pharmacol Exp Ther. 2017; 362(3):242-430

Drew KL, Jinka TR, Barati Z, Rice SA, Laughlin B, Bailey IR. “Translating CNS control of hibernation to no-hibernating species.” Autonomic Neuroscience: Basic and Clinical. 2015;192:1

## Mentoring
2017 - 2019: 		Michael Kaden-Hoffman.
			University of Alaska Fairbanks, Chemistry undergraduate. Michael   is 
			a premed student who has assisted with experiments and surgeries. He 
			was awarded an INBRE undergraduate summer fellowship award in 2018 
			to conduct research under my supervision over the summer. 

2016 – 2018:		Sierra Tagaban.
			University of Alaska Fairbanks, Chemistry undergraduate. Assisted with
surgeries and experiments. Completed undergraduate chemistry thesis under my supervision. She is currently a veterinary student. 

2014 – 2018: 	Katrina Dowel. 
University of Alaska Fairbanks, Biology undergraduate and Alaska Space Grant Recipient, Assisted with ECG analysis and stereotactic surgery. She is currently a medical student. 

2015-2016:		Katrina McCandless.
University of Alaska Fairbanks, Premedical undergraduate
Assisted with novel object recognition experiment and animal care.

2015- 2016:		Moriah Hunstiger.
University of Alaska Fairbanks, Chemistry honors undergraduate
Assisted with honors thesis project on memory. 

# Honors, Awards, and Research Support
5/2019:		INBRE Special Request Equipment Award $10,950   
2/2018:		BLaST Travel Award, $2000   
3/2017:	               INBRE Travel Award, $2000   
8/2016:		INBRE Travel Award, $133.33   
5/2016:		INBRE Travel Award, $2000    
5/2016:		BLaST Curriculum Development Award, $17,095.59    
4/2016:		BLaST Travel Award, $6241    
5/2016:		INBRE Travel Award, $2000    
6/2015:		INBRE Travel Award, $4400    
4/2015:		INBRE Travel Award, $2000    
9/2014:		INBRE Travel Award, $3100    

## Community Service/Outreach Activities 
Murie Open House   
Interior Alaska Science Fair  
Literacy Council of Alaska   
Volunteer Physician for Don Antonio Lugo High School Football Team   
Suitcase Clinic    